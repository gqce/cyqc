# CyQC: Quantum computing toolset for correlated materials simulations
A collection of codes which implement quantum algorithms for correlated materials simulations. They are developed by the [quantum computing for materials science team](https://scholar.google.com/citations?user=HbS_lmgAAAAJ&hl=en) in Ames National Laboratory. The repository is managed using git submodules.

## Codes in the collection
* `hva`: statevector implementation of variational quantum eigensolver (VQE) with Hamiltonian variational ansatz.
* `adaptvqe`: statevector implementation of qubit-ADAPT-VQE.
* `circuitadaptvqe`: circuit implementation of qubit-ADAPT-VQE for the (Ns=2, Nb=2) eg impurity model.
* `avqmetts`: statevector implementation of adaptive variational quantum minimally entangled typical thermal states for finite temperature simulations of quantum spin lattice models.
* `avqs_response_function`: parallel statevector implementation of adaptive variational quantum simulaiotns of Greens function and nolinear susceptibilities. 

## Associated publications
* A. Mukherjee, N. F. Berthusen, J. C. Getelina, P. P. Orth, and Y.-X. Yao, Comparative Study of Adaptive Variational Quantum Eigensolvers for Multi-Orbital Impurity Models, [Commun. Phys. 6, 1 (2023).](https://www.nature.com/articles/s42005-022-01089-6)
* J. C. Getelina, N. Gomes, T. Iadecola, P. P. Orth, and Y.-X. Yao, Adaptive Variational Quantum Minimally Entangled Typical Thermal States for Finite Temperature Simulations, [SciPost Physics 15, 102 (2023).](https://scipost.org/SciPostPhys.15.3.102)
* M. Mootz, T. Iadecola, and Y.-X. Yao, Adaptive Variational Quantum Computing Approaches for Green’s Functions and Nonlinear Susceptibilities, [J. Chem. Theory Comput. 20, 8689 (2024).](https://doi.org/10.1021/acs.jctc.4c00874)
