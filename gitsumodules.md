# Add a Git Submodule
``$ git submodule add [--name alias] <remote_url> <destination_folder>``
``$ git commit -m "Added the submodule to the project."``
``$ git push``

# Pull a Git Submodule
``git submodule update --init --recursive``

# Fetch new submodule commits
``$ cd repository/submodule``
``$ git fetch``

# Remove Git submodules
``$ git submodule deinit <submodule>``
``$ git rm <submodule>``

# Submodule summary
``$ git config --global status.submoduleSummary true``
